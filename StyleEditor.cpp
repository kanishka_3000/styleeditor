#include "StyleEditor.h"
#include "ui_StyleEditor.h"

#include <QMessageBox>
StyleEditor::StyleEditor(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::StyleEditor)
{
    p_Application = NULL;
    ui->setupUi(this);
}

StyleEditor::~StyleEditor()
{
    delete ui;
}



void StyleEditor::Initialize(QApplication *pApplication)
{
    QFile oFile(DEFAULT_STYLE_NAME);
    if(!oFile.open(QIODevice::ReadWrite))
    {
        QMessageBox::information(this,"Error","Can not open style");
        return;
    }

    QString sText = oFile.readAll();
    p_Application = pApplication;
    ui->p_TextEditor->setText(sText);
    p_Application->setStyleSheet(QString(sText));
    oFile.close();
}

void StyleEditor::OnSave()
{
    QString sText = ui->p_TextEditor->toPlainText();
     p_Application->setStyleSheet(QString(sText));
     QFile oFile(DEFAULT_STYLE_NAME);
     if(!oFile.open(QIODevice::ReadWrite))
     {
         QMessageBox::information(this,"Error","Can not open style");
         return;
     }
     oFile.write(qPrintable(sText));
     oFile.flush();
     oFile.close();
}
