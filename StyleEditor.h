#ifndef STYLEEDITOR_H
#define STYLEEDITOR_H

#include <QWidget>
#include <QFile>
#define DEFAULT_STYLE_NAME "style.css"
namespace Ui {
class StyleEditor;
}

class StyleEditor : public QWidget
{
    Q_OBJECT

public:
    explicit StyleEditor(QWidget *parent = 0);
    ~StyleEditor();
    void Initialize(QApplication* pApplication);

public slots:
    void OnSave();
private:
    Ui::StyleEditor *ui;
    QApplication* p_Application;
};

#endif // STYLEEDITOR_H
