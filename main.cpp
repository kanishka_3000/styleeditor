#include "StyleEditor.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    StyleEditor w;
    w.Initialize(&a);
    w.show();

    return a.exec();
}
