cmake_minimum_required(VERSION 2.8)

project(DynamicStyle)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOMOC ON)
SET(CMAKE_AUTOUIC ON)  

set(CMAKE_PREFIX_PATH "/opt/Qt5.5.0/5.5/gcc_64" ${CMAKE_PREFIX_PATH} )
find_package(Qt5Widgets REQUIRED)
find_package(Qt5Core REQUIRED)
find_package(Qt5Gui REQUIRED)
find_package(Qt5Xml REQUIRED)
find_package(Qt5XmlPatterns REQUIRED)
find_package(Qt5OpenGL REQUIRED)

SET(UI_HEADERS "")
SET(UI_RESOURCES "")

qt5_wrap_ui(UI_HEADERS StyleEditor.ui)
add_executable(${PROJECT_NAME} main.cpp StyleEditor.cpp)


#target_include_directories(${PROJECT_NAME} PUBLIC ${PROJECT_SOURCE_DIR})

target_link_libraries(${PROJECT_NAME} Qt5::Widgets)
target_link_libraries(${PROJECT_NAME} Qt5::Core)
target_link_libraries(${PROJECT_NAME} Qt5::Gui)


