#-------------------------------------------------
#
# Project created by QtCreator 2015-12-16T23:13:29
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QTDynamicStyleEditor
TEMPLATE = app


SOURCES += main.cpp\
        StyleEditor.cpp

HEADERS  += StyleEditor.h

FORMS    += StyleEditor.ui
